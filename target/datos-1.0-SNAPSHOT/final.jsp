<%-- 
    Document   : final
    Created on : 05-04-2022, 0:36:12
    Author     : pramos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Datos Finales</h1>
        <%
        String nombre = (String)request.getAttribute("nombre");
        String seccion = (String)request.getAttribute("seccion");
            %>
            
            <p>Hola <%=nombre%>, tu Sección es :<%=seccion%></p>
    </body>
</html>
